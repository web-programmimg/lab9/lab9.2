import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:5000'
})

function delay(sec: number) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(sec), sec * 300)
  })
}

instance.interceptors.response.use(
  async function (res) {
    await delay(1)
    return res
  },
  function (error) {
    return Promise.reject(error)
  }
)
export default instance
